DELETE FROM reservation;
DELETE FROM horaire;
DELETE FROM terrain;
DELETE FROM adherent;

INSERT INTO terrain VALUES
(101, 'tennis', 1),
(102, 'tennis', 2),
(103, 'tennis', 3),
(104, 'tennis', 4),
(201, 'badminton', 1),
(202, 'badminton', 2),
(203, 'badminton', 3),
(204, 'badminton', 4),
(301, 'squash', 1),
(302, 'squash', 2),
(303, 'squash', 3),
(304, 'squash', 4),
(305, 'squash', 5),
(306, 'squash', 6),
(307, 'squash', 7),
(308, 'squash', 8);

INSERT INTO adherent VALUES
(100, 'donatien', 'dupont', 'dodo.dup@yopmail.com', NULL),
(101, 'eugène', 'hulet', 'eugene.hulet@wanadoo.com', 100),
(102, 'patrick', 'balkany', 'patrick.balkany@fisc.gouv.fr', 100);

INSERT INTO horaire VALUES
('2021-09-07 10:00:00'),
('2021-09-08 11:00:00'),
('2021-09-08 12:00:00'),
('2021-09-08 14:00:00');

INSERT INTO reservation VALUES
(102, 204, '2021-09-07 10:00:00'),
(100, 104, '2021-09-08 11:00:00'),
(100, 104, '2021-09-08 12:00:00');