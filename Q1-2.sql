DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS horaire;
DROP TABLE IF EXISTS terrain;
DROP TABLE IF EXISTS adherent;

CREATE TABLE adherent
(
    id_adherent SERIAL,
    nom VARCHAR(80),
    prenom VARCHAR(80),
    mail VARCHAR(200),
    id_parrain INT,

    CONSTRAINT pk_adherent 
        PRIMARY KEY (id_adherent),

    CONSTRAINT fk_parrain
        FOREIGN KEY (id_parrain)
        REFERENCES adherent(id_adherent)
        ON DELETE SET NULL
        ON UPDATE CASCADE
);

CREATE TABLE terrain
(
    id_terrain SERIAL,
    sport VARCHAR(40),
    numero INT,

    CONSTRAINT pk_terrain 
        PRIMARY KEY (id_terrain)
);

CREATE TABLE horaire
(
    horaire TIMESTAMP,

    CONSTRAINT pk_horaire
        PRIMARY KEY (horaire)
);

CREATE TABLE reservation
(
    id_adherent INT NOT NULL,
    id_terrain INT NOT NULL,
    horaire TIMESTAMP NOT NULL,

    CONSTRAINT pk_reservation
        PRIMARY KEY (id_terrain, horaire),
    
    CONSTRAINT fk_terrain
        FOREIGN KEY (id_terrain)
        REFERENCES terrain(id_terrain)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    
    CONSTRAINT fk_horaire
        FOREIGN KEY (horaire)
        REFERENCES horaire(horaire)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);